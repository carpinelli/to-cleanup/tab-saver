# Tab Saver

Provides a launcher that uses PyAutoGUI to save Firefox tabs 
(and maybe other browsers if the shortcuts are similar enough) to a file. 
The same launcher can be used to open a Firefox browser with each tab loaded.

## Contact

mr.carpinelli@protonmail.ch

## Contributing

This is mostly a personal project, but any contributions are welcome and
appreciated, provided they align with the goals of the project.

## Building



## License

All contributions are made under the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html). See the [LICENSE](LICENSE).
