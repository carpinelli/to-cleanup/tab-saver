#!/usr/bin/env python3

from pathlib import Path
import threading
import time

import pyperclip
import pyautogui


def clipboard_capture(sleep_seconds, stop_event, clipboard_set) -> None:
    """Saves several clipboard copies to a set. Checks every 100ms"""
    while not stop_event.is_set():
        clipboard_set.add(pyperclip.paste())
        time.sleep(sleep_seconds)
    return None  # set *clipboard_set

def clipboard_aggregator() -> int:
    clipboard_set = set()
    text = " ".join(["The clipboard is monitored every 100ms, every unique change",
                     "will be add to a '\n' separated list. When you press 'OK', the",
                     "list will replace the current clipboard's contents."])


    stop_event = threading.Event()
    clipboard_capture_thread = threading.Thread(target=clipboard_capture,
                                                args=(0.1, stop_event, clipboard_set))
    clipboard_capture_thread.daemon = True
    clipboard_capture_thread.start()
    # Stop event.
    result = pyautogui.alert(text)
    stop_event.set()
    clipboard_capture_thread.join()
    print(clipboard_set)
    pyperclip.copy("\n".join(clipboard_set))

    return 0

if __name__ == "__main__":
    clipboard_aggregator()

