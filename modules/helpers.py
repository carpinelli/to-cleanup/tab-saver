#! /usr/bin/env python3

"""Helper functions for working with common filetypes and functions."""

import time
import json
import csv

from pathlib import Path


def countdown(Seconds: float = 5.0, Text: str = "{seconds}...") -> None:
    """Counts down from Seconds, alerting the user to the remaining
    seconds.
    Text must contain the substring '{seconds}'. If nothing
    is passed into Text, then just the remaining seconds are used."""
    for seconds in range(Seconds, 0, -1):
        print(Text.format(seconds=seconds))
        time.sleep(1.0)
    return None


def to_json(IterableObj: (list, tuple), savePath: (Path, str),
            Mode: str = 'w') -> None:
    """Writes an iterable to a JSON file using a human readable
    style. Raises an exception if unsuccessful."""
    savePath = Path(savePath)
    with open(savePath, Mode, encoding="utf-8") as json_out:
        json.dump(IterableObj, json_out, ensure_ascii=False, indent=4)
    return None


def to_csv(IterableObj: (list, tuple), savePath: (Path, str),
           Mode: str = 'w') -> None:
    """Writes an iterable to a CSV file. Raises an exception if
    unsuccessful."""
    with open(savePath, Mode, newline='') as csv_out:
        csv_writer = csv.writer(csv_out)
        csv_writer.writerow(IterableObj)
    return None


def load_json(loadPath: (Path, str)) -> (list, dict):
    """Returns a list or dict loaded from a JSON file specified by
    loadPath."""
    loadPath = Path(loadPath).expanduser().absolute()
    # Cast to Path, and ensure absolute path
    with open(loadPath, 'r') as json_in:
        return json.load(json_in)


def load_csv(loadPath: (Path, str)) -> list:
    """Returns a list loaded from a CSV file specified by
    loadPath."""
    loadPath = Path(loadPath).expanduser().absolute()
    # Cast to Path, and ensure absolute path
    with open(loadPath, 'r') as csv_in:
        reader = csv.reader(csv_in)  # Create CSV reader
        return list(reader)  # "[varname]" to remove outer list
